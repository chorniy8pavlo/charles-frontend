export enum UserRoles {
    USER = 'Regular user',
    ADMIN = 'Admin user',
}
