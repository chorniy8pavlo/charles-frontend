import { useJwt } from 'react-jwt';
import { getToken } from './accessToken';

export const useRoleAuth = () => {
    const { decodedToken } = useJwt(getToken());
    return {
        isAdmin: decodedToken?.roles.includes('ADMIN'),
    };
};
