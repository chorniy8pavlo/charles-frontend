import { Link } from "react-router-dom";
import { Form, Formik } from "formik";
import * as Yup from "yup";

import { signIn } from "@api/auth";

import { Field } from "@uikit/Fields";
import { Button } from "@uikit/Button";

export const Reset = () => {
  return (
    <div className="z-50 w-470 px-14 py-12 bg-white">
      <p className="mb-11 text-2xl">
        Reset password or{" "}
        <Link className="underline text-primary hover:text-primary hover:opacity-70 transition-all" to="/?auth=login">
          Login
        </Link>
      </p>
      <p className="text-base mb-11">Enter email used at registration</p>
      <Formik
        initialValues={{
          email: "",
          password: "",
        }}
        onSubmit={async (values) => {
          await signIn({
            username: values.email,
            password: values.password,
          }).then((res) => {
            // console.log(res);
            // if (res.authorized) {
            //   window.location.pathname = "/profile/dashboard";
            // }
          });
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string().email("Invalid email").required("Required"),
          password: Yup.string().required("Required"),
        })}
      >
        {({ errors, touched }) => (
          <Form className="space-y-11">
            <Field name="email" placeholder="Email" type="email" errors={errors} touched={touched} />
            <div className="flex justify-between items-center">
              <Button title="Send" />
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};
