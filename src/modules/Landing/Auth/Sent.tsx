import { Button } from "@uikit/Button";
import { Link } from "react-router-dom";

export const Sent = () => {
  return (
    <div className="z-50 w-470 px-14 py-12 bg-white">
      <p className="mb-11 text-2xl">Reset password</p>
      <p className="text-base mb-11">A recovery link has been sent to your email address. Please click on it to reset password.</p>
      <Link to="/">
        <Button title="Close" />
      </Link>
    </div>
  );
};
