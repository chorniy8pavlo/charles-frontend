import { Link } from "react-router-dom";
import { Form, Formik } from "formik";
import * as Yup from "yup";

import { Field } from "@uikit/Fields";
import { Button } from "@uikit/Button";

export const ResetForm = () => {
  return (
    <div className="z-50 w-470 px-14 py-12 bg-white">
      <p className="mb-11 text-2xl">Reset password</p>
      <Formik
        initialValues={{
          password: "",
          passwordConfirmation: "",
        }}
        onSubmit={async (values) => {}}
        validationSchema={Yup.object().shape({
          password: Yup.string().min(4, "Password length should be more 4 symbols").required("Required"),
          passwordConfirmation: Yup.string().oneOf([Yup.ref("password")], "Passwords should be same"),
        })}
      >
        {({ errors, touched }) => (
          <Form className="space-y-11">
            <Field name="password" placeholder="Password" type="password" errors={errors} touched={touched} />
            <Field name="passwordConfirmation" placeholder="Confirm  password" type="password" errors={errors} touched={touched} />
            <div className="flex justify-between items-center">
              <Button title="Submit" />
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};
