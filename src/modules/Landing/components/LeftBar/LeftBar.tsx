import facebook from './icons/facebook.svg';
import twitter from './icons/twitter.svg';
import instagram from './icons/instagram.svg';

const LeftBar = () => {
    return (
        <div className="h-full w-8 py-20 absolute top-0 left-7 flex flex-col items-center space-y-6 mmd:hidden">
            <div className="h-80 w-1 bg-black"></div>
            <div className="space-y-5 flex items-center flex-wrap justify-center">
                <img src={facebook} alt="" />
                <img src={twitter} alt="" />
                <img src={instagram} alt="" />
            </div>
            <div className="h-80 w-1 bg-black"></div>
        </div>
    );
};

export default LeftBar;
