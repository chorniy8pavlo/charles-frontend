import { useAuthorized } from '@utils/useAuthorized';
import { Link } from 'react-router-dom';

import { Spinner } from '@components/Spinner';

function Navbar() {
    const { authorized, loading } = useAuthorized();

    if (loading) return <Spinner />;

    return (
        <nav className="w-full flex justify-between px-60 items-center h-16 py-1 box-content">
            <div className="">
                <Link to="/" className="text-xl font-semibold">
                    LOGO
                </Link>
            </div>
            <div className="space-x-14">
                <Link to="/">Services</Link>
                <Link to="/">Experience</Link>
                <Link to="/">Studying process</Link>
                <Link to="/">Join</Link>
                <Link to="/">Contact</Link>
            </div>
            <div>
                {authorized ? (
                    <Link to="/profile/dashboard">My profile</Link>
                ) : (
                    <Link to="?auth=login">Login / Signup</Link>
                )}
            </div>
        </nav>
    );
}

export default Navbar;
