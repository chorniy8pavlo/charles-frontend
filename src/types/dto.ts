export interface IUser {
    _id: string;
    username: string;
    firstname: string;
    lastname: string;
    password: string;
    roles: string[];
    age: number;
    nativeLanguage: string;
    country: string;
    jobTitle: string;
    isAccountCompleted: boolean;
    transactions: any[];
    balance: number;
    rate: number;
    image: string;
    nickname: string;
    currency: 'USD' | 'EUR' | 'GBP';
    link: string;
}
