const path = require("path");
const resolvePath = p => path.resolve(__dirname, p)

module.exports = {
  webpack: {
    alias: {
      '@uikit': resolvePath('./src/modules/uikit'),
      '@api': resolvePath('./src/api/rest'),
      '@utils': resolvePath('./src/utils'),
      '@components': resolvePath('./src/components'),
    }
  },
  style: {
    postcss: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
      ],
    },
  },
}