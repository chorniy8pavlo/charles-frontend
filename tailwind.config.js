module.exports = {
    mode: 'jit',
    purge: ['./src/**/*.html', './src/**/*.tsx'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            width: {
                1080: '1080px',
                470: '470px',
                964: '964px',
                500: '500px',
                1: '1px',
            },
            height: {
                600: '600px',
                550: '550px',
                500: '500px',
                450: '450px',
            },
            screens: {
                m3xl: { max: '2000px' },
                // => @media (min-width: 2000px) { ... }

                m2xl: { max: '1535px' },
                // => @media (max-width: 1535px) { ... }

                mxl: { max: '1279px' },
                // => @media (max-width: 1279px) { ... }

                mlg: { max: '1025px' },
                // => @media (max-width: 1023px) { ... }

                mmd: { max: '767px' },
                // => @media (max-width: 767px) { ... }

                msm: { max: '639px' },
                // => @media (max-width: 639px) { ... }
            },
        },
        fontFamily: {
            futuraMedium: ['FuturaMediumC'],
            manrope: ['Manrope'],
            inter: ['Inter'],
        },
        colors: {
            primary: '#6997ff',
            black: '#303030',
            white: '#ffffff',
            grey: {
                light: '#C6C6C6',
                dark: 'rgba(0,0,0,0.5)',
            },
            error: '#FF6161',
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
};
